<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Nama: " . $sheep->type . "<br>";
echo "Legs: " . $sheep->legs . "<br>";
echo "Cold Blooded: " . $sheep->cold_blooded . "<br><br>";

$frog = new Frog("buduk");
echo "Nama: " . $frog->type . "<br>";
echo "Legs: " . $frog->legs . "<br>";
echo "Cold Blooded: " . $frog->cold_blooded . "<br>";
echo "Jump: " . $frog->jump() . "<br><br>";

$ape = new Ape("kera sakti");
echo "Nama: " . $ape->type . "<br>";
echo "Legs: " . $ape->legs . "<br>";
echo "Cold Blooded: " . $ape->cold_blooded . "<br>";
echo "Yell: " . $ape->yell() . "<br><br>";
